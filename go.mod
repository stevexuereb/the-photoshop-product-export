module gitlab.com/azzsteve/the-photoshop-product-export

go 1.16

require (
	github.com/go-logr/logr v0.4.0
	github.com/go-logr/zapr v0.4.0
	go.uber.org/zap v1.16.0
)
